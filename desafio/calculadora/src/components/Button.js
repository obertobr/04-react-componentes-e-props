export default function button({ value, adicionar}) {
  return (
    <button onClick={() => adicionar(value)}>{value}</button>
  );
}