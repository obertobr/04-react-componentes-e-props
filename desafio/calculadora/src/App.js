import './App.css';
import Input from './components/Input';
import Button from './components/Button';
import React, { useState } from 'react'

function App() {
  const [input, setInput] = useState(0)
  
  const Adicionar = (num) => {
    if(input == 0){
      setInput(num)
    } else {
      setInput(input + num)
    }
  }

  return (
    <div className="App">
      <table>
        <tr>
          <td colspan="4"><Input value={input} /></td>
        </tr>
        <tr>
          <td colspan="2"><button onClick={() => setInput(0)}>C</button></td>
          <td><button onClick={() => setInput(input.slice(0, -1))}>«</button></td>
          <td><Button value="+" adicionar={Adicionar}/></td>
        </tr>
        <tr>
          <td><Button value="7" adicionar={Adicionar}/></td>
          <td><Button value="8" adicionar={Adicionar}/></td>
          <td><Button value="9" adicionar={Adicionar}/></td>
          <td><Button value="-" adicionar={Adicionar}/></td>
        </tr>
        <tr>
          <td><Button value="4" adicionar={Adicionar}/></td>
          <td><Button value="5" adicionar={Adicionar}/></td>
          <td><Button value="6" adicionar={Adicionar}/></td>
          <td><Button value="*" adicionar={Adicionar}/></td>
        </tr>
        <tr>
          <td><Button value="1" adicionar={Adicionar}/></td>
          <td><Button value="2" adicionar={Adicionar}/></td>
          <td><Button value="3" adicionar={Adicionar}/></td>
          <td><Button value="/" adicionar={Adicionar}/></td>
        </tr>
        <tr>
          <td colspan="2"><Button value="0" adicionar={Adicionar}/></td>
          <td><Button value="." adicionar={Adicionar}/></td>
          <td><button onClick={() => setInput(eval(input))}>=</button></td>
        </tr>
      </table>
    </div>
  );
}

export default App;
